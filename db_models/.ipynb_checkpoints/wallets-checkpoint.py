from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, create_engine, select, join, MetaData, Table, insert,Sequence

from config_vars import BBDD_CONNECTION

Base = declarative_base()

secuencia = Sequence('SEQ_WALLETS')

class Wallets(Base):
    __tablename__ = "wallets"
    wallet_id = Column(Integer, secuencia, primary_key=True)

    print("Cargando Configuracion de billeteras")
    
    engine = create_engine(BBDD_CONNECTION)
    metadata = MetaData()
    wallets = Table("wallets", metadata, autoload=True, autoload_with=engine, schema='billetera_cristian')
    
    print("Configuracion Finalizada")

    @classmethod
    def all_wallets(cls):
        """
            Devolver Billeteras
        """
        query = select([cls.wallets])
        return query


    @classmethod
    def single_wallet(cls,*,id_wallet):
        """
            Devolver una billetera
        """
        query = select([cls.wallets]).where(cls.wallets.c.wallet_id == id_wallet)
        return query

    @classmethod
    def wallet_by_user(cls,*,id_user):
        """
            Devolver una billetera por id de usuario
        """
        query = select([cls.wallets]).where(cls.wallets.c.user_id == id_user)
        return query





