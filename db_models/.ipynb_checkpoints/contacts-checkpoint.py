from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, create_engine, select, join, MetaData, Table, insert,Sequence
from . import wallets

from config_vars import BBDD_CONNECTION

Base = declarative_base()

secuencia = Sequence('SEQ_CONTACTS')

class Contacts(Base):
    __tablename__ = "contacts"
    contact_id = Column(Integer, secuencia, primary_key=True)

    print("Cargando Configuracion de Contactos")
    
    engine = create_engine(BBDD_CONNECTION)
    metadata = MetaData()
    contacts = Table("contacts", metadata, autoload=True, autoload_with=engine, schema='billetera_cristian')
    
    print("Configuracion Finalizada")

    @classmethod
    def all_contacts(cls):
        """
            Devolver Contactos
        """
        query = select([cls.contacts])
        return query


    @classmethod
    def single_contact(cls,*,id_contact):
        """
            Devolver una Contacto
        """
        query = select([cls.contacts]).where(cls.contacts.c.contact_id == id_contact)
        return query

    @classmethod
    def contacts_by_wallet(cls,*,id_wallet):
        """
            Devolver una lista de contactos por id de wallet
        """
        query = select([cls.contacts]).where(cls.contacts.c.wallet_id == id_wallet)
        return query

    @classmethod
    def contacts_by_user(cls,*,id_user):
        """
            Devolver una lista de contactos por id de usuario
        """
        wallet = wallets.Wallets.wallets
        
        j = join(cls.contacts,wallet,cls.contacts.c.wallet_id == wallet.c.wallet_id)
        query = select([cls.contacts]).select_from(j).where(wallet.c.user_id == id_user)
        return query



