from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, create_engine, select, join, MetaData, Table, insert,Sequence

from config_vars import BBDD_CONNECTION

Base = declarative_base()

secuencia = Sequence('SEQ_TYPEOFTRANSACTIONS')

class TypeOfTransaction(Base):
    __tablename__ = "typeoftransaction"
    type_of_transaction_id = Column(Integer, secuencia, primary_key=True)

    print("Cargando Configuracion de tipos de transacciones")
    engine = create_engine(BBDD_CONNECTION)
    metadata = MetaData()
    
    tot = Table("typeoftransaction", metadata, autoload=True, autoload_with=engine, schema='billetera_cristian')
    
    print("Configuracion Finalizada")

    @classmethod
    def all_type_of_transaction(cls):
        query = select([cls.tot])
        return query

    @classmethod
    def single_type_of_transaction(cls,*,id_type_of_transaction):
        query = select([cls.tot]).where(cls.tot.c.type_of_transaction_id == id_type_of_transaction)
        return query
        
    