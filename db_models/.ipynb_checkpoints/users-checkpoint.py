from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, create_engine, select, join, MetaData, Table, insert,Sequence,func

from config_vars import BBDD_CONNECTION

Base = declarative_base()

class Users(Base):
    __tablename__ = "users"
    user_id = Column(Integer, primary_key=True)

    print("Cargando Configuracion de usuarios")
    engine = create_engine(BBDD_CONNECTION)
    metadata = MetaData()
    
    us = Table("users", metadata, autoload=True, autoload_with=engine, schema='billetera_cristian')
    
    print("Configuracion Finalizada")

    @classmethod
    def all_users(cls):
        """
            Devolver Usuarios
        """
        query = select([cls.us])
        return query

    @classmethod
    def single_user(cls,*,id):
        """
            Devolver un solo usuario
        """
        query = select([cls.us]).where(cls.us.c.user_id == id)
        return query

    @classmethod
    def users_by_last_name_and_similar_surname(cls,*,user):
        aux = func.CONCAT(cls.us.c.user_name,' ')
        aux1 = func.CONCAT(aux,cls.us.c.user_lastname)
        query = select([cls.us]).where(aux1.like(f'%{user}%'))
        return query

    @classmethod
    def insert_user(cls,user_name, user_lastname, user_email, user_password):
        """
            Insertar un nuevo usuario en la tablauser.
        """
        ins = cls.us.insert().values(user_name = user_name, user_lastname= user_lastname, user_email = user_email,user_password = user_password)
        return ins

    @classmethod
    def update_user(cls,id_user,user_name,user_lastname,user_email, user_password):
        """
            Actualiza un usuario
        """
        ins = cls.us.update().where(cls.us.c.user_id == id_user).values(user_name = user_name, user_lastname= user_lastname, user_email = user_email,user_password = user_password)
        return ins
    
    @classmethod
    def delete_user(cls,*,id):
        """
            Eliminar un solo usuario
        """
        query = cls.us.delete().where(cls.us.c.user_id == id)
        return query