from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, create_engine, select, join, MetaData, Table, insert,Sequence

from config_vars import BBDD_CONNECTION

Base = declarative_base()

secuencia = Sequence('SEQ_COMPANYS')

class Companys(Base):
    __tablename__ = "companys"
    company_id = Column(Integer, secuencia, primary_key=True)

    print("Cargando Configuracion de empresas")
    engine = create_engine(BBDD_CONNECTION)
    metadata = MetaData()
    
    companys = Table("companys", metadata, autoload=True, autoload_with=engine, schema='billetera_cristian')
    
    print("Configuracion Finalizada")

    @classmethod
    def all_companys(cls):
        query = select([cls.companys])
        return query


    @classmethod
    def single_company(cls,*,id_company):
        query = select([cls.companys]).where(cls.companys.c.company_id == id_company)
        return query
        