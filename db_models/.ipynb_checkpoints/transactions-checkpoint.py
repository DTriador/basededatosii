from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, create_engine, select, join, MetaData, Table, insert,Sequence
from . import wallets

from config_vars import BBDD_CONNECTION

Base = declarative_base()

secuencia = Sequence('SEQ_TRANSACTIONS')

class Transactions(Base):
    __tablename__ = "transactions"
    transaction_id = Column(Integer, secuencia, primary_key=True)

    print("Cargando Configuracion de transacciones")
    
    engine = create_engine(BBDD_CONNECTION)
    metadata = MetaData()
    trans = Table("transactions", metadata, autoload=True, autoload_with=engine, schema='billetera_cristian')
    
    print("Configuracion Finalizada")

    @classmethod
    def all_transactions(cls):
        query = select([cls.trans])
        return query
    
    @classmethod
    def single_transaction(cls,*,id_transaction):
        query = select([cls.trans]).where(cls.trans.c.transaction_id == id_transaction)
        return query
    
    @classmethod
    def transactions_by_wallet(cls,*,id_wallet):
        query=select([cls.trans]).where(cls.trans.c.wallet_id == id_wallet)
        return query
    

    @classmethod
    def transactions_by_user(cls,*,id_user):
        j = join(cls.trans , wallets.Wallets.wallets , cls.trans.c.wallet_id == wallets.Wallets.wallets.c.wallet_id)
        query = select([cls.trans]).select_from(j).where(wallets.Wallets.wallets.c.user_id == id_user)
        return query
