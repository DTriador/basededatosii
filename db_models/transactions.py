from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, create_engine, MetaData, Table, select, join
from config_vars import BBDD_CONNECTION

Base = declarative_base()

from . import users
from . import wallets
from . import typeOfTransaction
class Transactions(Base):
    __tablename__ = "transactions"
    print("entering parameters config")
    engine = create_engine(BBDD_CONNECTION)
    metadata = MetaData()
    transactions = Table("transactions", metadata, autoload=True, autoload_with=engine, schema='parcial2')
    id_not_in_db = Column(Integer, primary_key=True)
    print("finished config for parameters")
    
    @classmethod
    def single_transactions(cls, *, transactions_id):
        """ cual es la transaction con id """
        query = select([cls.transactions]).where(cls.transactions.c.transactions_id == transactions_id)
        return query
        
    @classmethod
    def all_transactions(cls):
        """ Cuáles son todas las transacciones """
        query = select([cls.transactions])
        return query

    @classmethod
    def transactions_by_user(cls, * , user_id):
        """ Cuáles son todas las transacciones del user usr_id """
        j = join(cls.transactions, wallets.Wallets.wallet,
                cls.transactions.c.wallet_id == wallets.Wallets.wallet.c.wallet_id,
            ).join(
                users.Users.user,
                wallets.Wallets.wallet.c.user_id
                == users.Users.user.c.user_id,
            )
        query = (select([cls.transactions]).select_from(j).where(users.Users.user.c.user_id == user_id))
        return query

    @classmethod
    def transactions_by_type_of_transactions(cls, * , tpe_id):
        """ Cuáles son todas las transacciones del tipo tipo de transaction tpe_id """ 
        j = join(cls.transactions, typeoftransactions.TypeOfTransactions.tpe, cls.transactions.c.tpe_id == typeoftransactions.TypeOfTransactions.tpe.c.tpe_id)
        query = (select([cls.transactions]).select_from(j).where(typeoftransactions.TypeOfTransactions.tpe.c.tpe_id == tpe_id))
        return query

    @classmethod
    def transactions_by_user_and_type_of_transactions(cls, * , user_id, tpe_id):
        """ Cuáles son todas las transacciones del user usr_id y el tipo de transaction tpe """
        j = join(cls.transactions, wallets.Wallets.wallet, cls.transactions.c.wallet_id == wallets.Wallets.wallet.c.wallet_id,
                ).join(
                    users.Users.user,
                    wallets.Wallets.wal.c.user_id == users.Users.user.c.user_id, 
                ).join(
                     typeoftransactions.TypeOfTransactions.tpe,
                    cls.transactions.c.tpe_id == typeoftransactions.TypeOfTransactions.tpe.c.tpe_id)
        query = (select([cls.transactions]).select_from(j).where(users.Users.user.c.user_id == user_id))
        return query