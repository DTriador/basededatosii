from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, create_engine, MetaData, Table, select, join
from config_vars import BBDD_CONNECTION

Base = declarative_base()
from . import users
from . import wallets

class Companies(Base):
    __tablename__ = "companies"
    print("entering parameters config")
    engine = create_engine(BBDD_CONNECTION)
    metadata = MetaData()
    companies = Table("companies", metadata, autoload=True, autoload_with=engine, schema='parcial2')
    id_not_in_db = Column(Integer, primary_key=True)
    print("finished config for parameters")
    
    @classmethod
    def single_companies(cls, *, companies_id):
        """ cual es la compañia con id """
        query = select([cls.companies]).where(cls.companies.c.companies_id == companies_id)
        return query
        
    @classmethod
    def all_companies(cls):
        """ Cuáles son todas las compañias """
        query = select([cls.companies])
        return query
        
    @classmethod
    def companies_by_user(cls, * , user_id):
        """ Cuáles son todas las companies del user usr_id """
        j = join(cls.companies, wallets.Wallets.wallet, cls.companies.c.wallet_id == wallets.Wallets.wallet.c.wallet_id).join(users.Users.user,
                wallets.Wallets.wallet.c.user_id
                == users.Users.user.c.user_id)
        query = (select([cls.companies]).select_from(j).where(users.Users.user.c.user_id == user_id))
        return query