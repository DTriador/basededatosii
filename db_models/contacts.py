from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, create_engine, MetaData, Table, select, join
from config_vars import BBDD_CONNECTION

Base = declarative_base()

from . import users
from . import wallets
class Contacts(Base):
    __tablename__ = "contacts"
    print("entering parameters config")
    engine = create_engine(BBDD_CONNECTION)
    metadata = MetaData()
    contacts = Table("contacts", metadata, autoload=True, autoload_with=engine, schema='parcial2')
    id_not_in_db = Column(Integer, primary_key=True)
    print("finished config for parameters")
    
    @classmethod
    def single_contacts(cls, *, contacts_id):
        """
        cual es el contacto con id
        """
        query = select([cls.contacts]).where(cls.contacts.c.contacts_id == contacts_id)
        return query
        
    @classmethod
    def all_contacts(cls):
        """
        Cuáles son todas los contactos
        """
        query = select([cls.contacts])
        return query

    @classmethod
    def contacts_by_user(cls, * , user_id):
        """ Cuáles son todos los contacts del user usr_id """
        j = join(cls.contacts, wallets.Wallets.wallet, cls.contacts.c.wallet_id == wallets.Wallets.wallet.c.wallet_id).join( users.Users.user, wallets.Wallets.wallet.c.user_id == users.Users.user.c.user_id)
        query = (select([cls.contacts]).select_from(j).where(users.Users.user.c.user_id == user_id))
        return query