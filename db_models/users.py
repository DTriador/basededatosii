from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, create_engine, MetaData, Table,join, select, insert, or_
from config_vars import BBDD_CONNECTION
from . import provinces
Base = declarative_base()


class Users(Base):
    __tablename__ = "users"
    print("entering parameters config")
    engine = create_engine(BBDD_CONNECTION)
    metadata = MetaData()
    user = Table("users", metadata, autoload=True, autoload_with=engine, schema='parcial2')
    #id_not_in_db = Column(Integer, primary_key=True)
    id_not_in_db = Column(Integer, primary_key=True)
    print("finished config for parameters")
    
    @classmethod
    def single_users(cls, *, user_id):
        # cual es el user con id 
        query = select([cls.user]).where(cls.user.c.user_id == user_id)
        return query
    
    @classmethod
    def users_by_name_or_last_name(cls,*,user_name):
        query = select([cls.user]).where(or_( cls.user.c.user_name.like(f'%{user_name}%'), cls.user.c.user_last_name.like(f'%{user_name}%')))
        return query
        
    @classmethod
    def all_users(cls):
        # cuáles son todos los tipos de transacciones
        query = select([cls.user])
        return query
        
    @classmethod
    def insert_user(cls, user_name, user_last_name, user_email, user_password,pro_id):
        # insertar un nuevo usuario
        insert_query = cls.user.insert().values(
            user_name=user_name,
            user_last_name=user_last_name,
            user_email=user_email,
            user_password=user_password,
            pro_id = pro_id)
        return insert_query
        
    @classmethod
    def update_user(cls, user_id, user_name, user_last_name,user_email, user_password, pro_id):
        # actulizar usuario
        update_query = cls.user.update().where(cls.user.c.user_id == user_id).values(
            user_name = user_name,
            user_last_name= user_last_name,
            user_email = user_email,
            user_password = user_password,
            pro_id = pro_id)
        return update_query
    
    @classmethod
    def delete_user(cls, user_id):
        # eliminar un usuario
        delete_query = cls.user.delete().where(cls.user.c.user_id == user_id)
        return delete_query
    
    #funcion que filtra usuarios por provincia
    @classmethod
    def users_by_provinces(cls,*,pro_id):
        query = select([cls.user]).where(cls.user.c.pro_id == pro_id)
        return query
    
    @classmethod
    def users_by_countries(cls,*,pro_pais):
        j = join(cls.user, provinces.Provinces.provinces, cls.user.c.pro_id ==  provinces.Provinces.provinces.c.pro_id)
        query = select([cls.user]).select_from(j).where(provinces.Provinces.provinces.c.pro_pais == pro_pais)
        return query