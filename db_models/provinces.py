from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, create_engine, MetaData, Table, select, insert, or_
from config_vars import BBDD_CONNECTION

Base = declarative_base()


class Provinces(Base):
    __tablename__ = "provinces"
    print("entering parameters config")
    engine = create_engine(BBDD_CONNECTION)
    metadata = MetaData()
    provinces = Table("provinces", metadata, autoload=True, autoload_with=engine, schema='parcial2')
    #id_not_in_db = Column(Integer, primary_key=True)
    id_not_in_db = Column(Integer, primary_key=True)
    print("finished config for parameters")
    
   