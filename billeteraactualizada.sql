-- Generado por Oracle SQL Developer Data Modeler 23.1.0.087.0806
--   en:        2023-11-03 17:44:29 ART
--   sitio:      Oracle Database 12cR2
--   tipo:      Oracle Database 12cR2
drop user parcial2 cascade;

CREATE USER parcial2 IDENTIFIED BY "Basededatos22023" ACCOUNT UNLOCK ;
GRANT CREATE SESSION TO parcial2;
ALTER USER parcial2 QUOTA 500M ON data;
GRANT CREATE TABLE TO parcial2;
GRANT CREATE SEQUENCE TO parcial2;

-- predefined type, no DDL - MDSYS.SDO_GEOMETRY

-- predefined type, no DDL - XMLTYPE

CREATE TABLE parcial2.companies (
    companies_id              NUMBER(10) NOT NULL,
    companies_holder_name     VARCHAR2(45 BYTE) NOT NULL,
    companies_number          VARCHAR2(20 BYTE),
    companies_company_name    VARCHAR2(45 BYTE) NOT NULL,
    companies_api_key         VARCHAR2(45 BYTE) NOT NULL,
    companies_expiration_date DATE NOT NULL,
    wallet_id                 NUMBER(10) NOT NULL
)
TABLESPACE data LOGGING NO INMEMORY;

CREATE UNIQUE INDEX parcial2.companies_pk ON
    parcial2.companies (
        companies_id
    ASC )
        TABLESPACE data LOGGING;

GRANT SELECT ON parcial2.companies TO wallet_dev;

ALTER TABLE parcial2.companies
    ADD CONSTRAINT companies_pk PRIMARY KEY ( companies_id )
        USING INDEX parcial2.companies_pk;

CREATE TABLE parcial2.contacts (
    contacts_id    NUMBER(10) NOT NULL,
    contacts_name  VARCHAR2(45 BYTE) NOT NULL,
    contacts_email VARCHAR2(45 BYTE) NOT NULL,
    wallet_id      NUMBER(10) NOT NULL
)
TABLESPACE data LOGGING NO INMEMORY;

CREATE UNIQUE INDEX parcial2.contacts_pk ON
    parcial2.contacts (
        contacts_id
    ASC )
        TABLESPACE data LOGGING;

GRANT SELECT ON parcial2.contacts TO wallet_dev;

ALTER TABLE parcial2.contacts
    ADD CONSTRAINT contacts_pk PRIMARY KEY ( contacts_id )
        USING INDEX parcial2.contacts_pk;

CREATE TABLE parcial2.transactions (
    transactions_id          NUMBER(10) NOT NULL,
    transactions_amount      NUMBER(11) NOT NULL,
    transactions_signe       VARCHAR2(45 BYTE) NOT NULL,
    transactions_date        DATE NOT NULL,
    transactions_description VARCHAR2(45 BYTE) NOT NULL,
    transactions_tracking    VARCHAR2(45 BYTE) NOT NULL,
    tpe_id                   NUMBER(10) NOT NULL,
    wallet_id                NUMBER(10) NOT NULL,
    companies_id             NUMBER(10)
)
TABLESPACE data LOGGING NO INMEMORY;

CREATE UNIQUE INDEX parcial2.transactions_pk ON
    parcial2.transactions (
        transactions_id
    ASC )
        TABLESPACE data LOGGING;

GRANT SELECT ON parcial2.transactions TO wallet_dev;

ALTER TABLE parcial2.transactions
    ADD CONSTRAINT transactions_pk PRIMARY KEY ( transactions_id )
        USING INDEX parcial2.transactions_pk;

CREATE TABLE parcial2.type_of_transactions (
    tpe_id   NUMBER(10) DEFAULT "ADMIN"."SEQ_TYPE_OF_TRANSACTIONS"."NEXTVAL" NOT NULL,
    tpe_name VARCHAR2(45 BYTE) NOT NULL
)
TABLESPACE data LOGGING NO INMEMORY;

CREATE UNIQUE INDEX parcial2.type_of_transactions_pk ON
    parcial2.type_of_transactions (
        tpe_id
    ASC )
        TABLESPACE data LOGGING;

GRANT SELECT ON parcial2.type_of_transactions TO wallet_dev;

ALTER TABLE parcial2.type_of_transactions
    ADD CONSTRAINT type_of_transactions_pk PRIMARY KEY ( tpe_id )
        USING INDEX parcial2.type_of_transactions_pk;

CREATE TABLE parcial2.provinces (
    pro_id   NUMBER(10) NOT NULL,
    pro_name VARCHAR2(100),
    pro_pais VARCHAR2(100)
)
LOGGING;

ALTER TABLE parcial2.provinces ADD CONSTRAINT provinces_pk PRIMARY KEY ( pro_id );

CREATE TABLE parcial2.users (
    user_id        NUMBER(10) NOT NULL,
    user_name      VARCHAR2(45 BYTE) NOT NULL,
    user_last_name VARCHAR2(45 BYTE) NOT NULL,
    user_email     VARCHAR2(45 BYTE) NOT NULL,
    user_password  VARCHAR2(45 BYTE) NOT NULL,
    pro_id         NUMBER(10) NOT NULL
)
TABLESPACE data LOGGING NO INMEMORY;

CREATE UNIQUE INDEX parcial2.users_pk ON
    parcial2.users (
        user_id
    ASC )
        TABLESPACE data LOGGING;

CREATE INDEX parcial2."inx_usr_pro_id " ON
    parcial2.users (
        pro_id
    ASC )
        TABLESPACE data LOGGING;
CREATE TABLE parcial2.wallets (
    wallet_id NUMBER(10) NOT NULL,
    user_id   NUMBER(10) NOT NULL
)
TABLESPACE data LOGGING NO INMEMORY;

CREATE UNIQUE INDEX parcial2.wallets_pk ON
    parcial2.wallets (
        wallet_id
    ASC )
        TABLESPACE data LOGGING;

GRANT SELECT ON parcial2.wallets TO wallet_dev;

ALTER TABLE parcial2.wallets
    ADD CONSTRAINT wallets_pk PRIMARY KEY ( wallet_id )
        USING INDEX parcial2.wallets_pk;

ALTER TABLE parcial2.companies
    ADD CONSTRAINT companies_wallets_fk FOREIGN KEY ( wallet_id )
        REFERENCES parcial2.wallets ( wallet_id )
    NOT DEFERRABLE;

ALTER TABLE parcial2.contacts
    ADD CONSTRAINT contacts_wallets_fk FOREIGN KEY ( wallet_id )
        REFERENCES parcial2.wallets ( wallet_id )
    NOT DEFERRABLE;

ALTER TABLE parcial2.transactions
    ADD CONSTRAINT transactions_companies_fk FOREIGN KEY ( companies_id )
        REFERENCES parcial2.companies ( companies_id )
    NOT DEFERRABLE;

ALTER TABLE parcial2.transactions
    ADD CONSTRAINT transactions_type_of_transactions_fk FOREIGN KEY ( tpe_id )
        REFERENCES parcial2.type_of_transactions ( tpe_id )
    NOT DEFERRABLE;

ALTER TABLE parcial2.transactions
    ADD CONSTRAINT transactions_wallets_fk FOREIGN KEY ( wallet_id )
        REFERENCES parcial2.wallets ( wallet_id )
    NOT DEFERRABLE;

ALTER TABLE parcial2.wallets
    ADD CONSTRAINT wallets_users_fk FOREIGN KEY ( user_id )
        REFERENCES parcial2.users ( user_id )
    NOT DEFERRABLE;


-- Creacion de secuencias que empiezn en 10 para no afectar los registros previamente cargados 
-- Secuencia para type_of_transactions
CREATE SEQUENCE parcial2.seq_type_of_transactions START WITH 10 INCREMENT BY 1 NOCACHE NOCYCLE;
-- Secuencia para users
CREATE SEQUENCE parcial2.seq_users START WITH 10 INCREMENT BY 1 NOCACHE NOCYCLE;
-- Secuencia para wallets
CREATE SEQUENCE parcial2.seq_wallets START WITH 10 INCREMENT BY 1 NOCACHE NOCYCLE;
-- Secuencia para companies
CREATE SEQUENCE parcial2.seq_companies START WITH 10 INCREMENT BY 1 NOCACHE NOCYCLE;
-- Secuencia para contacts
CREATE SEQUENCE parcial2.seq_contacts START WITH 10 INCREMENT BY 1 NOCACHE NOCYCLE;
-- Secuencia para transactions
CREATE SEQUENCE parcial2.seq_transactions START WITH 10 INCREMENT BY 1 NOCACHE NOCYCLE;
-- Secuencia para provincias
CREATE SEQUENCE parcial2.seq_provinces START WITH 10 INCREMENT BY 1 NOCACHE NOCYCLE;




insert into parcial2.type_of_transactions (tpe_id, tpe_name) VALUES(1, 'CASH');
insert into parcial2.type_of_transactions (tpe_id, tpe_name) VALUES(2, 'CARD');
insert into parcial2.type_of_transactions (tpe_id, tpe_name) VALUES(3, 'INVOICE');
insert into parcial2.type_of_transactions (tpe_id, tpe_name) VALUES(4, 'TRANSFER');
commit;


--Insertar datos en la tabla provincias
insert into parcial2.provinces (pro_id, pro_name, pro_pais) values (parcial2.seq_provinces.NEXTVAL, 'Santa Fe','Argentina');
insert into parcial2.provinces (pro_id, pro_name, pro_pais) values (parcial2.seq_provinces.NEXTVAL, 'Mendoza','Argentina');
insert into parcial2.provinces (pro_id, pro_name, pro_pais) values (parcial2.seq_provinces.NEXTVAL, 'San Luis','Argentina');
insert into parcial2.provinces (pro_id, pro_name, pro_pais) values (parcial2.seq_provinces.NEXTVAL, 'La Rioja','Argentina');
insert into parcial2.provinces (pro_id, pro_name, pro_pais) values (parcial2.seq_provinces.NEXTVAL, 'Medellin','Colombia');

--Insertar datos en la tabla de users
insert into parcial2.users (user_id, user_name, user_last_name, user_email, user_password, pro_id) VALUES(1, 'Daniela','Triador', 'danielatriador@gmail.com', '36965115',1);
insert into parcial2.users (user_id, user_name, user_last_name, user_email, user_password, pro_id) VALUES(2, 'Nara','Triador', 'naratriador@gmail.com', '49025661',2);
insert into parcial2.users (user_id, user_name, user_last_name, user_email, user_password, pro_id) VALUES(3, 'Gabriel','Triador', 'gabrieltriador@gmail.com', '38772486',1);
insert into parcial2.users (user_id, user_name, user_last_name, user_email, user_password,  pro_id) VALUES(4, 'Gabriela','Torres', 'gabrielatorres@gmail.com', '24058456',4);
insert into parcial2.users (user_id, user_name, user_last_name, user_email, user_password, pro_id) VALUES(5, 'Adrian','Gamester', 'adriangamester@gmail.com', '23597131',5);
insert into parcial2.users (user_id, user_name, user_last_name, user_email, user_password, pro_id) VALUES(6, 'Fausto','Triador', 'faustotriador@gmail.com', '50142369',1);
insert into parcial2.users (user_id, user_name, user_last_name, user_email, user_password, pro_id) VALUES(7, 'Julian','Triador', 'cucutriador@gmail.com', '52698417',3);
insert into parcial2.users (user_id, user_name, user_last_name, user_email, user_password, pro_id) VALUES(8, 'Yolanda','Coz', 'yolandacoz@gmail.com', '10514999',2);
commit;
SELECT COUNT(*) as from parcial2.users;

--Insertar datos en la tabla wallets
insert into parcial2.wallets (wallet_id, user_id) values (1, 1);
insert into parcial2.wallets (wallet_id, user_id) values (2, 2);
insert into parcial2.wallets (wallet_id, user_id) values (3, 3);
insert into parcial2.wallets (wallet_id, user_id) values (4, 4);
insert into parcial2.wallets (wallet_id, user_id) values (5, 5);
insert into parcial2.wallets (wallet_id, user_id) values (6, 6);
insert into parcial2.wallets (wallet_id, user_id) values (7, 7);
insert into parcial2.wallets (wallet_id, user_id) values (8, 8);
commit;


--Insertar datos en la tabla companies
insert into parcial2.companies (companies_id, companies_holder_name, companies_number, companies_company_name, companies_api_key, companies_expiration_date, wallet_id) 
values (1, 'DANIELA TRIADOR', '4140001234567899', 'Visa', 'api visa', sysdate, 1);
insert into parcial2.companies (companies_id, companies_holder_name, companies_number, companies_company_name, companies_api_key, companies_expiration_date, wallet_id) 
values (2, 'DANIELA TRIADOR', '5399011296584789', 'Mastercard', 'api mastercard', sysdate, 1);

insert into parcial2.companies (companies_id, companies_holder_name, companies_number, companies_company_name, companies_api_key, companies_expiration_date, wallet_id) 
values (3, 'GABRIELA TORRES', '4140001234565741', 'Visa', 'api visa', sysdate, 2);
insert into parcial2.companies (companies_id, companies_holder_name, companies_number, companies_company_name, companies_api_key, companies_expiration_date, wallet_id) 
values (4, 'GABRIELA TORRES', '5399011296584258', 'Mastercard', 'api mastercard', sysdate, 2);

insert into parcial2.companies (companies_id, companies_holder_name, companies_number, companies_company_name, companies_api_key, companies_expiration_date, wallet_id) 
values (5, 'GABRIEL TRIADOR', '4140001234561234', 'Visa', 'api visa', sysdate, 1);
insert into parcial2.companies (companies_id, companies_holder_name, companies_number, companies_company_name, companies_api_key, companies_expiration_date, wallet_id) 
values (6, 'GABRIEL TRIADOR', '53990112965848589', 'Mastercard', 'api mastercard', sysdate, 1);

insert into parcial2.companies (companies_id, companies_holder_name, companies_number, companies_company_name, companies_api_key, companies_expiration_date, wallet_id) 
values (7, 'NARA TRIADOR', '4140001234489741', 'Visa', 'api visa', sysdate, 2);
insert into parcial2.companies (companies_id, companies_holder_name, companies_number, companies_company_name, companies_api_key, companies_expiration_date, wallet_id) 
values (8, 'NARA TRIADOR', '5399011296384258', 'Mastercard', 'api mastercard', sysdate, 2);

commit;


--Insertar datos en la tabla contacts
insert into parcial2.contacts (contacts_id, contacts_name, contacts_email, wallet_id) values (1, 'nara', 'naratriador@gmail.com', 2);
insert into parcial2.contacts (contacts_id, contacts_name, contacts_email, wallet_id) values (2, 'gabriel', 'gabrieltriador@gmail.com', 3);
insert into parcial2.contacts (contacts_id, contacts_name, contacts_email, wallet_id) values (3, 'gabriela', 'gabrielatorres@gmail.com', 4);
insert into parcial2.contacts (contacts_id, contacts_name, contacts_email, wallet_id) values (4, 'adrian', 'adriangamester@gmail.com', 5);
insert into parcial2.contacts (contacts_id, contacts_name, contacts_email, wallet_id) values (5, 'fausto', 'faustotriador@gmail.com', 6);
insert into parcial2.contacts (contacts_id, contacts_name, contacts_email, wallet_id) values (6, 'julian', 'cucutriador@gmail.com', 7);
commit;


--Insertar datos en la tabla de type of transactions
insert into parcial2.transactions (transactions_id, transactions_amount, transactions_signe, transactions_date, transactions_description, transactions_tracking, tpe_id, wallet_id, companies_id)
values (1, 10000, '+', sysdate,'RAPIPAGO','12311', 1, 1, null);
insert into parcial2.transactions (transactions_id, transactions_amount, transactions_signe, transactions_date, transactions_description, transactions_tracking, tpe_id, wallet_id, companies_id)
values (2, 80000, '+', sysdate,'PAGOFACIL','12312', 2, 1, 1);
insert into parcial2.transactions (transactions_id, transactions_amount, transactions_signe, transactions_date, transactions_description, transactions_tracking, tpe_id, wallet_id, companies_id)
values (3, 50000, '-', sysdate,'CANTINA UNDEC','12313', 3, 1, null);
insert into parcial2.transactions (transactions_id, transactions_amount, transactions_signe, transactions_date, transactions_description, transactions_tracking, tpe_id, wallet_id, companies_id)
values (4, 10000, '-', sysdate,'naratriador@gmail.com','12314', 4, 1, null);

insert into parcial2.transactions (transactions_id, transactions_amount, transactions_signe, transactions_date, transactions_description, transactions_tracking, tpe_id, wallet_id, companies_id)
values (5, 2500, '+', sysdate,'HILAL','123211', 1, 2, null);
insert into parcial2.transactions (transactions_id, transactions_amount, transactions_signe, transactions_date, transactions_description, transactions_tracking, tpe_id, wallet_id, companies_id)
values (6, 7800, '-', sysdate,'CINTAS','12313', 3, 2, null);
insert into parcial2.transactions (transactions_id, transactions_amount, transactions_signe, transactions_date, transactions_description, transactions_tracking, tpe_id, wallet_id, companies_id)
values (7, 1000, '+', sysdate,'gabrielatorres@gmail.com','12314', 4, 2, null);

commit;



-- MOfiicar las tablas para asigna el valor de las secuanci por defecto en los primary key
-- Modificar la tabla type_of_transactions para usar la secuencia
ALTER TABLE parcial2.type_of_transactions MODIFY (tpe_id DEFAULT seq_type_of_transactions.NEXTVAL);
-- Modificar la tabla users para usar la secuencia
ALTER TABLE parcial2.users MODIFY (user_id DEFAULT seq_users.NEXTVAL);
-- Modificar la tabla wallets para usar la secuencia
ALTER TABLE parcial2.wallets MODIFY (wallet_id DEFAULT seq_wallets.NEXTVAL);
-- Modificar la tabla companies para usar la secuencia
ALTER TABLE parcial2.companies MODIFY (companies_id DEFAULT seq_companies.NEXTVAL);
-- Modificar la tabla contacts para usar la secuencia
ALTER TABLE parcial2.contacts MODIFY (contacts_id DEFAULT seq_contacts.NEXTVAL);
-- Modificar la tabla transactions para usar la secuencia
ALTER TABLE parcial2.transactions MODIFY (transactions_id DEFAULT seq_transactions.NEXTVAL);
-- Modificar la tabla provinces para usar la secuencia
ALTER TABLE parcial2.provinces MODIFY (pro_id DEFAULT seq_provinces.NEXTVAL);

ALTER TABLE parcial2.users
    ADD CONSTRAINT users_provinces_fk FOREIGN KEY ( pro_id )
        REFERENCES parcial2.provinces ( pro_id )
    NOT DEFERRABLE;

--Cree un usuario nuevo con la primera letra de su nombre y su apellido
--Asigne el privilegio de conexión al usuario creado y otorgue los privilegios 
--de select, insert y delete sobre todas las tablas del esquema PARCIAL2 

drop user dtriador cascade;


CREATE USER dtriador IDENTIFIED BY "Basededatos22023" DEFAULT TABLESPACE data;
GRANT CONNECT TO dtriador;
GRANT SELECT ON parcial2.users TO dtriador;
GRANT SELECT ON parcial2.wallets TO dtriador;
GRANT SELECT ON parcial2.companies TO dtriador;
GRANT SELECT ON parcial2.contacts TO dtriador;
GRANT SELECT ON parcial2.transactions TO dtriador;
GRANT SELECT ON parcial2.type_of_transactions TO dtriador;
GRANT SELECT ON parcial2.provinces TO dtriador;

GRANT INSERT ON parcial2.users TO dtriador;
GRANT INSERT ON parcial2.wallets TO dtriador;
GRANT INSERT ON parcial2.companies TO dtriador;
GRANT INSERT ON parcial2.contacts TO dtriador;
GRANT INSERT ON parcial2.transactions TO dtriador;
GRANT INSERT ON parcial2.type_of_transactions TO dtriador;
GRANT INSERT ON parcial2.provinces TO dtriador;

GRANT DELETE ON parcial2.users TO dtriador;
GRANT DELETE ON parcial2.wallets TO dtriador;
GRANT DELETE ON parcial2.companies TO dtriador;
GRANT DELETE ON parcial2.contacts TO dtriador;
GRANT DELETE ON parcial2.transactions TO dtriador;
GRANT DELETE ON parcial2.type_of_transactions TO dtriador;
GRANT DELETE ON parcial2.provinces TO dtriador;


--Asigne el privilegio de select al usuario creado sobre las secuencias de PARCIAL2 

GRANT SELECT ON seq_type_of_transactions TO dtriador;
GRANT SELECT ON seq_users TO dtriador;
GRANT SELECT ON seq_wallets TO dtriador; 
GRANT SELECT ON seq_companies TO dtriador;
GRANT SELECT ON seq_contacts TO dtriador; 
GRANT SELECT ON seq_transactions TO dtriador; 
GRANT SELECT ON seq_provinces TO dtriador; 
