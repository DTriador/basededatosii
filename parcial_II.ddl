-- Generado por Oracle SQL Developer Data Modeler 23.1.0.087.0806
--   en:        2023-11-04 09:24:12 ART
--   sitio:      Oracle Database 12cR2
--   tipo:      Oracle Database 12cR2



CREATE TABLESPACE data 
--  WARNING: Tablespace has no data files defined 
 LOGGING ONLINE
    EXTENT MANAGEMENT LOCAL AUTOALLOCATE
FLASHBACK ON;

CREATE USER billeteraintegrador 
    IDENTIFIED BY  
    ACCOUNT UNLOCK 
;

GRANT CREATE TABLE,
    CREATE SEQUENCE,
    CREATE SESSION
TO billeteraintegrador 
;

-- predefined type, no DDL - MDSYS.SDO_GEOMETRY

-- predefined type, no DDL - XMLTYPE

CREATE SEQUENCE billeteraintegrador.seq_companies INCREMENT BY 1 MAXVALUE 9999999999999999999999999999 MINVALUE 1 NOCACHE;

CREATE SEQUENCE billeteraintegrador.seq_contacts INCREMENT BY 1 MAXVALUE 9999999999999999999999999999 MINVALUE 1 NOCACHE;

CREATE SEQUENCE billeteraintegrador.seq_countries INCREMENT BY 1 MAXVALUE 9999999999999999999999999999 MINVALUE 1 CACHE 20;

CREATE SEQUENCE billeteraintegrador.seq_provinces;

CREATE SEQUENCE billeteraintegrador.seq_transactions INCREMENT BY 1 MAXVALUE 9999999999999999999999999999 MINVALUE 1 NOCACHE;

CREATE SEQUENCE billeteraintegrador.seq_type_of_transactions INCREMENT BY 1 MAXVALUE 9999999999999999999999999999 MINVALUE 1 NOCACHE;

CREATE SEQUENCE billeteraintegrador.seq_users INCREMENT BY 1 MAXVALUE 9999999999999999999999999999 MINVALUE 1 NOCACHE;

CREATE SEQUENCE billeteraintegrador.seq_wallets INCREMENT BY 1 MAXVALUE 9999999999999999999999999999 MINVALUE 1 NOCACHE;

CREATE TABLE billeteraintegrador.companies (
    companies_id              NUMBER(10) DEFAULT "ADMIN"."SEQ_COMPANIES"."NEXTVAL" NOT NULL,
    companies_holder_name     VARCHAR2(45 BYTE) NOT NULL,
    companies_number          VARCHAR2(20 BYTE),
    companies_company_name    VARCHAR2(45 BYTE) NOT NULL,
    companies_api_key         VARCHAR2(45 BYTE) NOT NULL,
    companies_expiration_date DATE NOT NULL,
    wallet_id                 NUMBER(10) NOT NULL
)
TABLESPACE data LOGGING NO INMEMORY;

CREATE UNIQUE INDEX billeteraintegrador.companies_pk ON
    billeteraintegrador.companies (
        companies_id
    ASC )
        TABLESPACE data LOGGING;

GRANT SELECT ON billeteraintegrador.companies TO wallet_dev;

ALTER TABLE billeteraintegrador.companies
    ADD CONSTRAINT companies_pk PRIMARY KEY ( companies_id )
        USING INDEX billeteraintegrador.companies_pk;

CREATE TABLE billeteraintegrador.contacts (
    contacts_id    NUMBER(10) DEFAULT "ADMIN"."SEQ_CONTACTS"."NEXTVAL" NOT NULL,
    contacts_name  VARCHAR2(45 BYTE) NOT NULL,
    contacts_email VARCHAR2(45 BYTE) NOT NULL,
    wallet_id      NUMBER(10) NOT NULL
)
TABLESPACE data LOGGING NO INMEMORY;

CREATE UNIQUE INDEX billeteraintegrador.contacts_pk ON
    billeteraintegrador.contacts (
        contacts_id
    ASC )
        TABLESPACE data LOGGING;

GRANT SELECT ON billeteraintegrador.contacts TO wallet_dev;

ALTER TABLE billeteraintegrador.contacts
    ADD CONSTRAINT contacts_pk PRIMARY KEY ( contacts_id )
        USING INDEX billeteraintegrador.contacts_pk;

CREATE TABLE billeteraintegrador.countries (
    cou_id   NUMBER(10) NOT NULL,
    cou_name VARCHAR2(40 BYTE)
)
TABLESPACE data LOGGING NO INMEMORY;

CREATE UNIQUE INDEX billeteraintegrador.countries_pk ON
    billeteraintegrador.countries (
        cou_id
    ASC )
        TABLESPACE data LOGGING;

ALTER TABLE billeteraintegrador.countries
    ADD CONSTRAINT countries_pk PRIMARY KEY ( cou_id )
        USING INDEX billeteraintegrador.countries_pk;

CREATE TABLE billeteraintegrador.provinces (
    pro_id   NUMBER(10) NOT NULL,
    pro_name VARCHAR2(100),
    pro_pais VARCHAR2(100)
)
LOGGING;

ALTER TABLE billeteraintegrador.provinces ADD CONSTRAINT provinces_pk PRIMARY KEY ( pro_id );

CREATE TABLE billeteraintegrador.transactions (
    transactions_id          NUMBER(10) DEFAULT "ADMIN"."SEQ_TRANSACTIONS"."NEXTVAL" NOT NULL,
    transactions_amount      NUMBER(11) NOT NULL,
    transactions_signe       VARCHAR2(45 BYTE) NOT NULL,
    transactions_date        DATE NOT NULL,
    transactions_description VARCHAR2(45 BYTE) NOT NULL,
    transactions_tracking    VARCHAR2(45 BYTE) NOT NULL,
    tpe_id                   NUMBER(10) NOT NULL,
    wallet_id                NUMBER(10) NOT NULL,
    companies_id             NUMBER(10)
)
TABLESPACE data LOGGING NO INMEMORY;

CREATE UNIQUE INDEX billeteraintegrador.transactions_pk ON
    billeteraintegrador.transactions (
        transactions_id
    ASC )
        TABLESPACE data LOGGING;

GRANT SELECT ON billeteraintegrador.transactions TO wallet_dev;

ALTER TABLE billeteraintegrador.transactions
    ADD CONSTRAINT transactions_pk PRIMARY KEY ( transactions_id )
        USING INDEX billeteraintegrador.transactions_pk;

CREATE TABLE billeteraintegrador.type_of_transactions (
    tpe_id   NUMBER(10) DEFAULT "ADMIN"."SEQ_TYPE_OF_TRANSACTIONS"."NEXTVAL" NOT NULL,
    tpe_name VARCHAR2(45 BYTE) NOT NULL
)
TABLESPACE data LOGGING NO INMEMORY;

CREATE UNIQUE INDEX billeteraintegrador.type_of_transactions_pk ON
    billeteraintegrador.type_of_transactions (
        tpe_id
    ASC )
        TABLESPACE data LOGGING;

GRANT SELECT ON billeteraintegrador.type_of_transactions TO wallet_dev;

ALTER TABLE billeteraintegrador.type_of_transactions
    ADD CONSTRAINT type_of_transactions_pk PRIMARY KEY ( tpe_id )
        USING INDEX billeteraintegrador.type_of_transactions_pk;

CREATE TABLE billeteraintegrador.users (
    user_id        NUMBER(10) DEFAULT "ADMIN"."SEQ_USERS"."NEXTVAL" NOT NULL,
    user_name      VARCHAR2(45 BYTE) NOT NULL,
    user_last_name VARCHAR2(45 BYTE) NOT NULL,
    user_email     VARCHAR2(45 BYTE) NOT NULL,
    user_password  VARCHAR2(45 BYTE) NOT NULL,
    cou_id         NUMBER(10),
    pro_id         NUMBER(10) NOT NULL
)
TABLESPACE data LOGGING NO INMEMORY;

CREATE UNIQUE INDEX billeteraintegrador.users_pk ON
    billeteraintegrador.users (
        user_id
    ASC )
        TABLESPACE data LOGGING;

CREATE INDEX billeteraintegrador."inx_usr_pro_id " ON
    billeteraintegrador.users (
        pro_id
    ASC )
        TABLESPACE data LOGGING;

GRANT SELECT ON billeteraintegrador.users TO wallet_dev;

ALTER TABLE billeteraintegrador.users
    ADD CONSTRAINT users_pk PRIMARY KEY ( user_id )
        USING INDEX billeteraintegrador.users_pk;

CREATE TABLE billeteraintegrador.wallets (
    wallet_id NUMBER(10) DEFAULT "ADMIN"."SEQ_WALLETS"."NEXTVAL" NOT NULL,
    user_id   NUMBER(10) NOT NULL
)
TABLESPACE data LOGGING NO INMEMORY;

CREATE UNIQUE INDEX billeteraintegrador.wallets_pk ON
    billeteraintegrador.wallets (
        wallet_id
    ASC )
        TABLESPACE data LOGGING;

GRANT SELECT ON billeteraintegrador.wallets TO wallet_dev;

ALTER TABLE billeteraintegrador.wallets
    ADD CONSTRAINT wallets_pk PRIMARY KEY ( wallet_id )
        USING INDEX billeteraintegrador.wallets_pk;

ALTER TABLE billeteraintegrador.companies
    ADD CONSTRAINT companies_wallets_fk FOREIGN KEY ( wallet_id )
        REFERENCES billeteraintegrador.wallets ( wallet_id )
    NOT DEFERRABLE;

ALTER TABLE billeteraintegrador.contacts
    ADD CONSTRAINT contacts_wallets_fk FOREIGN KEY ( wallet_id )
        REFERENCES billeteraintegrador.wallets ( wallet_id )
    NOT DEFERRABLE;

ALTER TABLE billeteraintegrador.transactions
    ADD CONSTRAINT transactions_companies_fk FOREIGN KEY ( companies_id )
        REFERENCES billeteraintegrador.companies ( companies_id )
    NOT DEFERRABLE;

ALTER TABLE billeteraintegrador.transactions
    ADD CONSTRAINT transactions_type_of_transactions_fk FOREIGN KEY ( tpe_id )
        REFERENCES billeteraintegrador.type_of_transactions ( tpe_id )
    NOT DEFERRABLE;

ALTER TABLE billeteraintegrador.transactions
    ADD CONSTRAINT transactions_wallets_fk FOREIGN KEY ( wallet_id )
        REFERENCES billeteraintegrador.wallets ( wallet_id )
    NOT DEFERRABLE;

ALTER TABLE billeteraintegrador.users
    ADD CONSTRAINT users_countries_fk FOREIGN KEY ( cou_id )
        REFERENCES billeteraintegrador.countries ( cou_id )
    NOT DEFERRABLE;

ALTER TABLE billeteraintegrador.users
    ADD CONSTRAINT users_provinces_fk FOREIGN KEY ( pro_id )
        REFERENCES billeteraintegrador.provinces ( pro_id )
    NOT DEFERRABLE;

ALTER TABLE billeteraintegrador.wallets
    ADD CONSTRAINT wallets_users_fk FOREIGN KEY ( user_id )
        REFERENCES billeteraintegrador.users ( user_id )
    NOT DEFERRABLE;



-- Informe de Resumen de Oracle SQL Developer Data Modeler: 
-- 
-- CREATE TABLE                             8
-- CREATE INDEX                             8
-- ALTER TABLE                             16
-- CREATE VIEW                              0
-- ALTER VIEW                               0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          8
-- CREATE MATERIALIZED VIEW                 0
-- CREATE MATERIALIZED VIEW LOG             0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        1
-- CREATE USER                              1
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- 
-- ORDS DROP SCHEMA                         0
-- ORDS ENABLE SCHEMA                       0
-- ORDS ENABLE OBJECT                       0
-- 
-- ERRORS                                   0
-- WARNINGS                                 1
