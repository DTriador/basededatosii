-- Generado por Oracle SQL Developer Data Modeler 23.1.0.087.0806
--   en:        2023-11-03 18:16:53 ART
--   sitio:      Oracle Database 12cR2
--   tipo:      Oracle Database 12cR2

-- predefined type, no DDL - MDSYS.SDO_GEOMETRY

-- predefined type, no DDL - XMLTYPE

CREATE SEQUENCE billeteraintegrador.seq_countries;

CREATE TABLE billeteraintegrador.countries (
    cou_id_number INTEGER NOT NULL,
    cou_name      VARCHAR2 (40)
--  ERROR: VARCHAR2 size not specified 

)
LOGGING;

ALTER TABLE billeteraintegrador.countries ADD CONSTRAINT countries_pk PRIMARY KEY ( cou_id_number );

GRANT SELECT ON billeteraintegrador.users TO wallet_dev;


ALTER TABLE billeteraintegrador.users
    ADD CONSTRAINT users_countries_fk FOREIGN KEY ( cou_id_number )
        REFERENCES billeteraintegrador.countries ( cou_id_number )
    NOT DEFERRABLE;


insert into billeteraintegrador.countries (cou_id_number, cou_name) values (billeteraintegrador.seq_countries.NEXTVAL, 'Argentina');
-- Informe de Resumen de Oracle SQL Developer Data Modeler: 
-- 
-- CREATE TABLE                             2
-- CREATE INDEX                             1
-- ALTER TABLE                              3
-- CREATE VIEW                              0
-- ALTER VIEW                               0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          1
-- CREATE MATERIALIZED VIEW                 0
-- CREATE MATERIALIZED VIEW LOG             0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              1
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- 
-- ORDS DROP SCHEMA                         0
-- ORDS ENABLE SCHEMA                       0
-- ORDS ENABLE OBJECT                       0
-- 
-- ERRORS                                   1
-- WARNINGS                                 0
