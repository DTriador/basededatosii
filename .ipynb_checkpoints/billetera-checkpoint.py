#from sqlalchemy import create_engine, select, join, MetaData, Table
import sys
import os
dirname = os.path.dirname(__file__)
 
sys.path.append(dirname)
sys.path.append(dirname+"/db_models/")
from sqlalchemy import create_engine, select, join, MetaData, Table

from db_models.users import Users
from db_models.companys import Companys
from db_models.typeOfTransaction import TypeOfTransaction
from db_models.wallets import Wallets
from db_models.contacts import Contacts
from db_models.transactions import Transactions
from config_vars import BBDD_CONNECTION

class Billetera:
    print("Iniciando")
    engine = create_engine(BBDD_CONNECTION)
    connection = engine.connect()
    print("Conexion Finalizada")

    def get_users(self,id = None,user = None):
        if(user and not id):
            query = Users.users_by_last_name_and_similar_surname(user=user)
        elif(id and not user):
            query = Users.single_user(id = id)
        else:
            query = Users.all_users()
        return self.connection.execute(query).fetchall()


    def insert_user(self,user_name, user_lastname, user_email, user_password):
        query = Users.insert_user(
            user_name=user_name,
            user_lastname=user_lastname,
            user_email=user_email,
            user_password=user_password
        )
        return self.connection.execute(query)

    def update_user(self,id_user,user_name, user_lastname, user_email, user_password):
        query = Users.update_user(
            id_user = id_user,
            user_name=user_name,
            user_lastname=user_lastname,
            user_email=user_email,
            user_password=user_password
        )
        return self.connection.execute(query)

    def delete_user(self,id):
        query = Users.delete_user(id = id)
        return self.connection.execute(query)


    def get_wallets(self,id_wallet = None, id_user = None):
        if(id_wallet is not None and id_user is None):
            query = Wallets.single_wallet(id_wallet = id_wallet)
        elif (id_wallet is None and id_user is not None):
            query = Wallets.wallet_by_user(id_user = id_user)
        elif (id_wallet is not None and is_user is not None):
            query = Wallets.single_wallet(id_wallet = id_wallet)
        else:
            query = Wallets.all_wallets()
        return self.connection.execute(query).fetchall()

    def get_contacts(self,id_contact = None, id_wallet = None,id_user = None):
        if(id_wallet is not None and id_contact is None and id_user is None):
            query = Contacts.contacts_by_wallet(id_wallet = id_wallet)
        elif (id_wallet is None and id_contact is not None and is_user is None):
            query = Contacts.single_contact(id_contact = id_contact)
        elif(id_wallet is None and id_contact is None and id_user is not None):
            query = Contacts.contacts_by_user(id_user = id_user)
        else:
            query = Contacts.all_contacts()
        return self.connection.execute(query).fetchall()

    def get_transactions(self,id_transaction = None,id_wallet=None,id_user=None):
        if(id_transaction and not id_wallet and not id_user):
            query = Transactions.single_transaction(id_transaction = id_transaction)
        elif(id_wallet and not id_transaction and not id_user):
            query = Transactions.transaction_by_wallet(id_wallet = id_wallet)
        elif(id_user and not id_wallet and not id_transaction):
            query = Transactions.transaction_by_user(id_user = id_user)
        else:
            query = Transactions.all_transactions()
        return self.connection.execute(query).fetchall()

    def get_types_of_transaction(self,id_type_of_transaction = None):
        if(id_type_of_transaction):
            query = TypeOfTransaction.single_type_of_transaction(id_type_of_transaction=id_type_of_transaction)
        else:
            query = TypeOfTransaction.all_type_of_transaction()
        return self.connection.execute(query).fetchall()

    def get_companys(self,id_company = None):
        if(id_company):
            query = Companys.single_company(id_company=id_company)
        else:
            query = Companys.all_companys()
        return self.connection.execute(query).fetchall()
    
